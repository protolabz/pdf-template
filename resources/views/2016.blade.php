<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Document</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/template.css')}}">
	<style>
</style>
</head>
<body>

<header>
	<div class="container">
		<div class="row">
			<div  class="col-md-5 col-sm-5 col-xs-5 m-top-30 text-center top-left">
				<h4 style="font-size:22px;" class="m-top-40 weight-300">ΠΑΝΕΠΙΣΤΗΜΙΟ ΠΕΙΡΑΙΩΣ</h4>
				<h4 style="font-size:22px;" class="m-top-20 weight-300">ΚΑΡΑΟΛΗ &amp; ΔΗΜΗΤΡΙΟΥ 80, 185 34, ΠΕΙΡΑΙΑΣ</h4>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2 text-center top-center">
				<img src="{{asset('img/unipilogo.png')}}" alt="">
			</div>
			<div  class="col-md-5 col-sm-5 col-xs-5 text-center">
				<h4 style="font-size:22px;" class="m-top-40 weight-300">UNIVERSITY OF PIRAEUS</h4>
				<h4 style="font-size:22px;" class="m-top-20 weight-300">80 KARAOLI &amp; DIMITRIOY Str, 18534, PIRAEUS</h4>
			</div>
		</div>
	</div>
</header>
<br><br><br><br><br><br><br><br><br><br><br><br>
<main style="padding-top:37px;">
	<div class="container" style="margin:0 60px;">
		<div class="row">
			<div style="" class="col-xs-12 col-sm-12 text-center">
				<h1 class="title font-45">ΒΕΒΑΙΩΣΗ ΠΑΡΑΚΟΛΟΥΘΗΣΗΣ</h1>
				<p class="font-20 m-top-20">Με την παρούσα βεβαιώνεται ότι ο/η :</p>
				<h1 class="name">John Doe</h1>
				<p class="font-20 m-top-20">Παρακολούθησε το συνέδριο</p>
				<h1 class="font-45 m-top-20">BUSINESS WEEK 2016</h1>

				<h3 class="m-top-20 weight-300" style="font-size:37px;">«Η Αρχιτεκτονική της Λήψης των Αποφάσεων: H Συμπεριφορική Διάσταση»</h3>

				<p class="m-top-30 desc">που διοργανώθηκε από το Σύλλογο Φοιτητών και Αποφοίτων του Προγράμματος
					Μεταπτυχιακών Σπουδών στη Διοίκηση
					Επιχειρήσεων του Πανεπιστημίου Πειραιώς "MBA's Society", σε συνεργασία με το Πρόγραμμα Μεταπτυχιακών
					Σπουδών στη
					Διοίκηση Επιχειρήσεων "MBA" και πραγματοποιήθηκε από τις 23 έως τις 27 Μαΐου 2016 στο Πανεπιστήμιο
					Πειραιώς</p>
			</div>
		</div>
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		<br><br><br><br><br><br><br><br><br><br>
		<div class="row" >
			<div  class="col-md-3 col-sm-3 col-xs-3 text-center">
				<h4 class="font-24 m-top-40 weight-300">MBA ’s Society</h4>
				<div class="mba-stamp">
					<img src="{{URL::to('img/mba-stamp.png')}}" alt="">
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-3 pull-right text-center">
				<h4 class="font-24 m-top-40 weight-300">Για το ΜΒΑ<br>Ο Διευθυντής</h4>
				<h4 class="font-24 m-top-50 weight-300">Λεωνίδας Χυτήρης<br>Καθηγητής</h4>
				<div class="unipi-stamp">
					<img src="{{URL::to('img/unipi-stamp.png')}}" alt="">
					<img class="signature" src="{{URL::to('img/signature.png')}}" alt="">
				</div>
			</div>
		</div>
	</div>
</main>
</body>
</html>